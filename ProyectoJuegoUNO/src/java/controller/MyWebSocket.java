package controller;

//codigo servidor

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import model.Utils;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
@ServerEndpoint(value = "/ws/feedaction")
public class MyWebSocket {
    
    public static ArrayList<Session> sessions = new ArrayList<>();
    
    @OnOpen
    public void onOpen(Session session) {

        session.setMaxIdleTimeout(0);
        sessions.add(session);
        //sessions.put(IDENTIFICADOR, session);

    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("sale");

        sessions.remove(session);

    }

    @OnMessage
    public void onMessage(Session session, String s) throws IOException {
        System.out.println(s);
        sendMessageToUser(s);
        //esto sería un ECO!
        //session.getBasicRemote().sendText(Utils.toJson(s));


    }

    @OnError
    public void onError(Session s, Throwable t) {
        t.printStackTrace();
    }

    public static void sendMessageToUser(String message) {
        
        for (int i=0; i<sessions.size();i++){
            try {
                sessions.get(i).getBasicRemote().sendText(message);
            } catch (IOException ex) {
                Logger.getLogger(MyWebSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }
    }


}
